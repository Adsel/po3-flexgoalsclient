# Get started #

### Install node.js ###
from *https://nodejs.org/en/download/*

### Install Angular via npm  ###
```sh
npm install -g @angular/cli
```

### Clone web-client's repository ###
```sh
git clone https://Adsel@bitbucket.org/Adsel/po3-flexgoalsclient.git
```

### Open project's directory ###
```sh
cd po3-flexgoalsclient/FlexGoalsClient
```

### Install dependencies and libraries via npm ###
```sh
npm install
```

### Run application with default settings ###
```sh
ng serve
```

### Open your browser and write URL address: ###
*http://localhost:4200/*

# App Usage #

1. Register account on website
![VS CODE](./docs/register.gif)

2. Log in to your account
![VS CODE](./docs/login.gif)

3. Adding targets
	1. quantitative goal
![VS CODE](./docs/add_q.gif)

	2. final goal
![VS CODE](./docs/add_f.gif)

3. Edditing targets
	1. quantitative goal
![VS CODE](./docs/edit_q.gif)

	2. final goal
![VS CODE](./docs/edit_f.gif)

4. Scoring targets (10 points per goal)
	1. quantitative goal
![VS CODE](./docs/prog_q.gif)
	
	2. final goal
![VS CODE](./docs/prog_f.gif)
	
5. Using paths	
![VS CODE](./docs/path.gif)

6. Progress preview
	1. quantitative goal
![VS CODE](./docs/prev_q.gif)
	
	2. final goal
![VS CODE](./docs/prev_f.gif)

7. Deleting targets
	1. quantitative goal
![VS CODE](./docs/del_q.gif)	
	
	2. final goal
![VS CODE](./docs/del_f.gif)

8. Edit your personal information
![VS CODE](./docs/edit.gif)

# Used Technologies #

### node.js ###
https://nodejs.org/en/

### Angular ###
https://angular.io/

### Bootstrap ###
https://getbootstrap.com/

### ng-bootstrap ###
https://ng-bootstrap.github.io/

### SASS ###
https://sass-lang.com/

### Creative Tim | blk design system angular ###
Licencja MIT
https://www.creative-tim.com/product/blk-design-system-angular

### Visual Studio Code ###
https://code.visualstudio.com/

### Inteliij idea ###
https://www.jetbrains.com/idea/

### Postman ###
https://www.postman.com/