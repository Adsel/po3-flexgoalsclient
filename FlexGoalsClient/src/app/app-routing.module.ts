import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './users/welcome/welcome.component';
import { AuthGuard } from './core/auth-guard.service';
import { UserLoginComponent } from './users/user-login/user-login.component';
import { UserRegisterComponent } from './users/user-register/user-register.component';
import { AddTargetComponent } from './targets/add-target/add-target.component';
import { UserEditDataComponent } from './users/user-edit-data/user-edit-data.component';
import { PreviewTargetComponent } from './targets/preview-target/preview-target.component';
import { IndexComponent } from './users/index/index.component';

const routes: Routes = [
  { path: "home", component: IndexComponent },
  { path: 'login', component: UserLoginComponent },
  { path: 'register', component: UserRegisterComponent },
  { path: 'prev-goal', component: PreviewTargetComponent, canActivate: [AuthGuard] },
  { path: 'edit-data', component: UserEditDataComponent, canActivate: [AuthGuard] },
  { path: 'welcome', component: WelcomeComponent, canActivate: [AuthGuard] },
  { path: 'add-goal', component: AddTargetComponent, canActivate: [AuthGuard] },
  { path: '**', component: IndexComponent }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: []
})
export class AppRoutingModule {}
