import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {User, UserData} from 'src/app/model';
import { AuthService } from 'src/app/core/auth.service';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';
import {ToastrService} from "ngx-toastr";
import {ShowTargetsComponent} from "../../targets/show-targets/show-targets.component";


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit,OnDestroy {

  @ViewChild(ShowTargetsComponent)
  private child: ShowTargetsComponent;
  isCollapsed = true;
  user: User;
  edit: boolean;
  public date = new Date();

  constructor(
    private authService: AuthService,
    private router: Router,
    private userService: UsersService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.edit = false;
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");


   this.user = this.authService.currentUser;
     if(!this.authService.isLogged){
      this.router.navigate(['login']);
    }
  }

  logout() {
    this.authService.logout();
  }

  editUserData(){
    this.router.navigate(['edit-data']);
  }

  refreshPoints(){
    this.userService.getPoints().subscribe(
      response => {
        this.user.points = response;
        console.log("Udało się zaktualizować punkty!");
      },
      error => {
        console.log("Nie udało się zaktualizować punktów!");
      }
    );
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  dateToString(date: Date){
    let nmb: number = parseInt(date.toString().charAt(9)) + 1;
    let dateStr = date.toString().substr(0, 9) + nmb;
    return dateStr;
  }

  onShift(code: string){
    if(code === "done"){
      this.refreshPoints();
    }
    this.edit = false;
  }

  onShiftT(code:string){
    this.edit = true;
  }

  refreshGoals(code: string){
    this.child.loadFinalGoals();
    this.child.loadQuantitativeGoals();
  }
}
