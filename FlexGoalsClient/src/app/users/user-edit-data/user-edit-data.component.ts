import { Component, OnInit } from '@angular/core';
import { UserData } from 'src/app/model';
import { UsersService } from '../users.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-edit-data',
  templateUrl: './user-edit-data.component.html',
  styleUrls: ['./user-edit-data.component.scss']
})
export class UserEditDataComponent implements OnInit {
  public old: UserData;
  public nameS: string;
  public sexS: boolean;
  public dateS: String;

  constructor(
    public authService: AuthService,
    public userService: UsersService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(){
    console.log("DI " + this.authService.currentUser.id); 
    this.nameS = "";
    this.sexS = false;
    this.dateS = "";
    this.userService.getUserDataPrivate().subscribe(
      response => {
        this.nameS = response.name;
        this.sexS = response.sex;
        let temp = this.dateToString(response.date_of_birth);
        if(temp != "1800-01-01" && temp != "1799-12-32"){
          this.dateS = temp;
        }
        else{
          this.dateS = '';
        }
        this.old = response;
        console.log("Udało się pobrać dane osobiste użytkownika!");
      },
      error => {
        this.toastr.warning("Nie udało się pobrać danych osobistych!");
      }
    );
  }

  backToWelcome(){
    this.router.navigate(['welcome']);
  }

  dateToString(date: Date){
    let nmb: number = parseInt(date.toString().charAt(9)) + 1; 
    let dateStr = date.toString().substr(0, 9) + nmb;
    return dateStr;
  }

  savePrivData() {
    if(this.old) {
      if(this.nameS && this.old.name != this.nameS){
        this.old.name = this.nameS;
        console.log("b  " + this.nameS);
        this.userService.updateUserName(this.old).subscribe(
          response => {
            this.backToWelcome();
            console.log("Udało się zaktualizować imię!");
          },
          error => {
            this.toastr.warning("Nie udało się zaktualizować imienia!");
          }
        );
      }
      if(this.dateS && this.dateS != '' && this.dateS != this.dateToString(this.old.date_of_birth) && this.dateS != "1800-01-01"){
        this.old.date_of_birth = new Date(this.dateS.toString());
        this.userService.updateUserDate(this.old).subscribe(
          response => {
            this.backToWelcome();
            console.log("Udało się zaktualizować datę urodzenia!");
          },
          error => {
            this.toastr.warning("Nie udało się zaktualizować daty urodzenia!");
          }
        );
      }
      if(this.sexS && this.sexS != this.old.sex){
        this.old.sex = this.sexS;
        this.userService.updateUserSex(this.old).subscribe(
          response => {
            this.backToWelcome();
            console.log("Udało się zaktualizować płeć!");
          },
          error => {
            this.toastr.warning("Nie udało się zaktualizować płci!");
          }
        );
      }
    }
      if(!this.old){
        if(this.nameS != ''){
          this.userService.updateUserName({
            name: this.nameS,
            sex: null,
            date_of_birth: null,
            id: this.authService.currentUser.id
          }).subscribe(
            response => {
              this.backToWelcome();
              console.log("Udało się zaktualizować imię!");
            },
            error => {
              this.toastr.warning("Nie udało się zaktualizować imienia!");
            }
          );
        }
        if(this.dateS != ''){
          this.userService.updateUserDate({
            name: null,
            sex: null,
            date_of_birth: new Date(this.dateS.toString()),
            id: this.authService.currentUser.id
          }).subscribe(
            response => {
              this.backToWelcome();
              console.log("Udało się zaktualizować datę urodzenia!");
            },
            error => {
              this.toastr.warning("Nie udało się zaktualizować daty urodzenia!");
            }
          );
        }
        this.userService.updateUserSex({
          name: null,
          sex: this.sexS,
          date_of_birth: null,
          id: this.authService.currentUser.id
        }).subscribe(
          response => {
            this.backToWelcome();
            console.log("Udało się zaktualizować płeć!");
          },
          error => {
            this.toastr.warning("Nie udało się zaktualizować płci!");
          }
        );
      }
    }
  
}
