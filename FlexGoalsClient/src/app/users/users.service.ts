import { Injectable, Inject } from '@angular/core';
import { Config, CONFIG, UserData } from '../model';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../core/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    @Inject(CONFIG) private config: Config,
    private httpClient: HttpClient,
    private authService: AuthService
  ) { }

  getUserDataPrivate(){
    return this.httpClient.get<UserData>(
      `${this.config.apiUrl}/users/priv-data/${this.authService.currentUser.id}`
    );
  }

  updateUserName(data: UserData): Observable<UserData> {
    return this.httpClient.put<UserData>(
      `${this.config.apiUrl}/users/priv-data/update-name`,
      data,
      { headers: this.config.headerConfig }
    );
  }

  updateUserDate(data: UserData): Observable<UserData> {
    return this.httpClient.put<UserData>(
      `${this.config.apiUrl}/users/priv-data/update-date`,
      data,
      { headers: this.config.headerConfig }
    );
  }

  updateUserSex(data: UserData): Observable<UserData> {
    return this.httpClient.put<UserData>(
      `${this.config.apiUrl}/users/priv-data/update-sex`,
      data,
      { headers: this.config.headerConfig }
    );
  }

  getPoints() {
    return this.httpClient.get<number>(
      `${this.config.apiUrl}/users/points/${this.authService.currentUser.id}`
    );
  }
}
