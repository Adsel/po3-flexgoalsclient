import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLoginComponent } from './user-login/user-login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FormsModule } from '@angular/forms';
import { UserRegisterComponent } from './user-register/user-register.component';
import { TargetsModule } from '../targets/targets.module';
import { UserEditDataComponent } from './user-edit-data/user-edit-data.component';
import { UsersService } from './users.service';
import {RouterModule} from "@angular/router";
import {BsDatepickerModule, CollapseModule, ModalModule, BsDropdownModule, ProgressbarModule, TooltipModule, PopoverModule, TabsModule, PaginationModule, AlertModule, CarouselModule} from "ngx-bootstrap";
import { IndexComponent } from './index/index.component';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';

@NgModule({
  declarations: [
    UserLoginComponent,
    WelcomeComponent,
    UserRegisterComponent,
    UserRegisterComponent,
    UserEditDataComponent,
    IndexComponent,

  ],
    imports: [
        CommonModule,
        FormsModule,
        TargetsModule,
        RouterModule,
        CollapseModule,
        BsDatepickerModule,
        ModalModule,
        BsDropdownModule.forRoot(),
        ProgressbarModule.forRoot(),
        TooltipModule.forRoot(),
        PopoverModule.forRoot(),
        CollapseModule.forRoot(),
        JwBootstrapSwitchNg2Module,
        TabsModule.forRoot(),
        PaginationModule.forRoot(),
        AlertModule.forRoot(),
        BsDatepickerModule.forRoot(),
        CarouselModule.forRoot(),
        ModalModule.forRoot()
    ],
  exports: [
    UserLoginComponent,
    WelcomeComponent,
    UserRegisterComponent,
    UserEditDataComponent
  ]
})
export class UsersModule { }


