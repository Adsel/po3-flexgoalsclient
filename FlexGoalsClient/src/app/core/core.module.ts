import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './auth-guard.service';
import { HttpClientModule, HttpHeaders } from '@angular/common/http'
import { Config, CONFIG } from '../model';

const config: Config = {
  apiUrl: 'http://51.68.136.95:8080/api',
  headerConfig : new HttpHeaders().set('Content-Type', 'application/json')
}

@NgModule({
  providers: [
    { provide: CONFIG, useValue: config },
    AuthGuard
  ],
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ]
})
export class CoreModule { }
