import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Config, User, AuthData, CONFIG } from '../model';

import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { error } from 'protractor';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLogged: boolean;
  currentUser: User;

  constructor(
    private httpClient: HttpClient,
    @Inject(CONFIG) private config: Config,
    private toastr: ToastrService,
    private router: Router
  ) { }

  login(login: string, password: string) {    
      this.checkAuth(login, password).subscribe(
        response => {
          console.log("ODPOWIEDŹ Z SERWERA = ");
          console.log(response);
          
          this.isLogged = true;
          this.toastr.success("Zalogowano!!");
          this.router.navigate(['welcome']);
          this.currentUser = response;
        },
        error => {
          console.log("Nieudane logowanie - spróbuj ponownie!");
          this.toastr.warning("Nieudana próba logowania!");
        }
      );
  }

  private checkAuth(login: string, password: string){
    return this.httpClient.post<User>(
      `${this.config.apiUrl}/users/login-user`, 
      { "login" : login, "password" : password }, 
      { headers: this.config.headerConfig }
    );
  }

  logout() {
    this.currentUser = null;
    this.isLogged = false;
    this.router.navigate(['login']);
  }

  register(login: string, email: string, password: string) {
    this.httpClient.post(
      `${this.config.apiUrl}/users/register-user`, 
      { "login" : login, "password" : password, "email" : email }, 
      { headers: this.config.headerConfig }
    ).subscribe(
      response => {
        this.toastr.success("Utworzono konto użytkownika!!");
        this.router.navigate(['login']);
      },
      error => {
        this.toastr.error("Nie udało się utworzyć konta!");
        this.toastr.warning("Spróbuj ponownie później!");
      }
    );
  }
}