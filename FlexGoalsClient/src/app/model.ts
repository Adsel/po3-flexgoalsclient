import { InjectionToken } from "@angular/core";
import { HttpHeaders } from '@angular/common/http';

export interface Config {
    apiUrl: string,
    headerConfig: HttpHeaders
}

export interface User {
    id: number,
    login: string,
    password: string,
    email: string,
    points: number
  }

export interface AuthData {
    login: string,
    password: string
}

// CEL ZALICZENIOWY
export interface PredefinedFinalGoal {
    id: number,
    name: string,
    description: string,
    goal: string,
    days: number
};

export interface FinalGoal {
    id: number,
    id_user: number,
    date: Date,
    is_shared: boolean,
    name: string,
    description: string,
    goal: string,
    days: number,
    points: number,
    progress: string
}

export interface FinalGoalFlag {
    id: number,
    id_user: number,
    date: Date,
    is_shared: boolean,
    name: string,
    description: string,
    goal: string,
    days: number,
    points: number,
    progress: string,
    flag: number
}

export interface FinalGoalData { // służy do wysłania danych na serwer
    id_user: number,
    is_shared: boolean,
    name: string,
    description: string,
    goal: string,
    days: number
}
// CEL ILOŚCIOWY 
export interface PredefinedQuantitativeGoal {
    id: number,
    name: string,
    description: string,
    goal: string,
    days: number,
    target: number,
    step: number
};

export interface QuantitativeGoal {
    id: number,
    id_user: number,
    date: Date,
    is_shared: boolean,
    name: string,
    description: string,
    goal: string,
    days: number,
    points: number,
    progress: string,
    target: number,
    step: number
};

export interface QuantitativeGoalFlag {
    id: number,
    id_user: number,
    date: Date,
    is_shared: boolean,
    name: string,
    description: string,
    goal: string,
    days: number,
    points: number,
    progress: string,
    target: number,
    step: number,
    flag: number
};

export interface QuantitativeGoalData {
    id_user: number,
    is_shared: boolean,
    name: string,
    description: string,
    goal: string,
    days: number,
    step: number
}

export interface QuantitativeGoalProgress {
    id: number,
    val: number
}

export interface UserData {
    id: number,
    name: string,
    date_of_birth: Date,
    sex: boolean
}

export interface ServerDate {
    year: number,
    month: number,
    day: number
}

// ŚCIEŻKI
export interface Path {
    id: number,
    name: string,
    description: string,
    goal: string,
    days: number,
    step: number,
    path: number,
    type: boolean
}

export interface PathData {
    id_user: number,
    id_path: number
}

export const CONFIG = new InjectionToken<Config>('app.config');