import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { QuantitativeGoal, FinalGoal, ServerDate, QuantitativeGoalFlag, FinalGoalFlag, FinalGoalData } from 'src/app/model';
import { AuthService } from 'src/app/core/auth.service';
import { TargetsService } from '../targets.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'tar-show-targets',
  templateUrl: './show-targets.component.html',
  styleUrls: ['./show-targets.component.scss']
})
export class ShowTargetsComponent implements OnInit {
  quantitativeGoals: QuantitativeGoalFlag[];
  finalGoals: FinalGoalFlag[];

  editableFGoal: FinalGoal
  editableQGoal: QuantitativeGoal;

  inputVal: number;
  @Output() shift = new EventEmitter<string>();

  constructor(
    public authService: AuthService, 
    public targetService: TargetsService,
    public toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.inputVal = 0;
    this.loadFinalGoals();
    this.loadQuantitativeGoals();
  }

  setInputVal(i: any){
    this.inputVal = i.target.value;
  }

  redirectToAddGoal(){
    this.router.navigate(['add-goal']);
  }

  deleteFinalGoal(id: number){
    this.targetService.deleteFinalGoal(id).subscribe(
      response => {
        this.toastr.success("Cel zaliczeniowy usunięty!");
        this.loadFinalGoals();
      },
      error => {
        this.toastr.warning("Nie udało się usunąć celu!");
      }
    );
  }

  deleteQuantitativeGoal(id: number){
    this.targetService.deleteQuantitativeGoal(id).subscribe(
      response => {
        this.toastr.success("Cel ilościowy usunięty!");
        this.loadQuantitativeGoals();
      },
      error => {
        this.toastr.warning("Nie udało się usunąć celu!");
      }
    );
  }

  @Output() shifts = new EventEmitter<string>();

  private emit() {
    this.shifts.emit('edit');
  }

  loadFinalGoals(){
    this.targetService.getFinalGoals(this.authService.currentUser.id).subscribe(
      response => {
        this.finalGoals = response;
        console.log("Udało się pobrać cele zaliczeniowe!");
      },
      error => {
        this.toastr.warning("Nie udało się pobrać celów zaliczeniowych!");
      }
    );
  }

  loadQuantitativeGoals() {
    this.targetService.getQuantitativeGoals(this.authService.currentUser.id).subscribe(
      response => {
        this.quantitativeGoals = response;
        console.log("Udało się pobrać cele ilościowe!");
      },
      error => {
        this.toastr.warning("Nie udało się pobrać celów ilościowych!");
      }
    );
  }

  editFinalGoal(goal: FinalGoal) {
    this.editableFGoal = goal;
    this.emit();
  }

  editQuantitativeGoal(goal: QuantitativeGoal){
    this.editableQGoal = goal;
    this.emit();
  }

  onShift(flag: string){
    this.editableQGoal = null;
    this.editableFGoal = null;
  }

  doneFinalGoal(f: number){

    this.targetService.doneFinalGoal(f).subscribe(
      response => {
        this.loadFinalGoals();
        this.shift.emit('done');
        console.log("Udało się zaliczyć cel zaliczeniowy!");
      },
      error => {
        this.toastr.warning("Nie udało się zaliczyć celu zaliczeniowego!");
      }
    );
  }

  doneQuantitativeGoal(id: number){
    console.log("aaa" + this.inputVal.toString());

    if(this.inputVal > 0){
      this.targetService.doneQuantitativeGoal({
        id: id,
        val: this.inputVal
      }).subscribe(
        response => {
          this.loadQuantitativeGoals();
          this.shift.emit('done');
          console.log("Udało się zaliczyć cel ilościowy!");
        },
        error => {
          this.toastr.warning("Nie udało się zaliczyć celu ilościowego!");
        }
      );
    }
  }

  previewFGoal(g: FinalGoal){
    this.router.navigate(['prev-goal', { id: g.id , type: 'f'}]);
  }

  previewQGoal(g: QuantitativeGoal){
    this.router.navigate(['prev-goal', { id: g.id , type: 'q'}]);
  }
}
