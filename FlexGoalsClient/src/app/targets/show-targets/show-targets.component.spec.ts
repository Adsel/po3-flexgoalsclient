import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTargetsComponent } from './show-targets.component';

describe('ShowTargetsComponent', () => {
  let component: ShowTargetsComponent;
  let fixture: ComponentFixture<ShowTargetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTargetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTargetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
