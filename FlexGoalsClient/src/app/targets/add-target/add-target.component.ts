import { Component, OnInit } from '@angular/core';
import { TargetsService } from '../targets.service';
import { AuthService } from 'src/app/core/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { PredefinedFinalGoal, FinalGoal, PredefinedQuantitativeGoal, QuantitativeGoal } from 'src/app/model';

@Component({
  selector: 'tar-add-target',
  templateUrl: './add-target.component.html',
  styleUrls: ['./add-target.component.scss']
})
export class AddTargetComponent implements OnInit {
  nameInput: string;
  descriptionInput: string;
  stepInput: number;
  goalInput: string;
  daysInput: number;
  sharedInput: boolean;

  isQuantitative: boolean;
  predefinedFinalGoals: PredefinedFinalGoal[];
  sharedFinalGoals: FinalGoal[];
  predefinedQuantitativeGoals: PredefinedQuantitativeGoal[];
  sharedQuantitativeGoals: QuantitativeGoal[];

  constructor(
    public authService: AuthService,
    public targetService: TargetsService,
    public toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.sharedInput = false;
    this.isQuantitative = true;
    this.targetService.getPredefinedFinalGoals().subscribe(
      response => {
        this.predefinedFinalGoals = response;
        console.log("Udało się pobrać PREDEFINIOWANE cele zaliczeniowe!");
      },
      error => {
        this.toastr.warning("Nie udało się pobrać PREDEFINIOWANYCH celów zaliczeniowych!");
      }
    );

    this.targetService.getPredefinedQuantitativeGoals().subscribe(
      response => {
        this.predefinedQuantitativeGoals = response;
        console.log("Udało się pobrać PREDEFINIOWANE cele ilościowe!");
      },
      error => {
        this.toastr.warning("Nie udało się pobrać PREDEFINIOWANYCH celów ilościowych!");
      }
    );

    this.targetService.getSharedFinalGoals(this.authService.currentUser.id).subscribe(
      response => {
        this.sharedFinalGoals = response;
        console.log("Udało się pobrać UDOSTĘPNIONE cele zaliczeniowe!");
      },
      error => {
        this.toastr.warning("Nie udało się pobrać UDOSTĘPNIONYCH celów zaliczeniowych!");
      }
    );

    this.targetService.getSharedQuantitativeGoals(this.authService.currentUser.id).subscribe(
      response => {
        this.sharedQuantitativeGoals = response;
        console.log("Udało się pobrać UDOSTĘPNIONE cele zaliczeniowe!");
      },
      error => {
        this.toastr.warning("Nie udało się pobrać UDOSTĘPNIONYCH celów zaliczeniowych!");
      }
    );
  }

  backToWelcome(){
    this.router.navigate(['welcome']);
  }

  useFGoal(goal: PredefinedFinalGoal | PredefinedQuantitativeGoal | FinalGoal | QuantitativeGoal) {
    this.nameInput = goal.name;
    this.descriptionInput = goal.description;
    this.goalInput = goal.description;
    this.daysInput = goal.days;
  }

  adjustFGoal(goal: FinalGoal | PredefinedFinalGoal){
    this.nameInput = goal.name;
    this.descriptionInput = goal.description;
    this.goalInput = goal.description;
    this.daysInput = goal.days;
  }

  adjustQGoal(goal: QuantitativeGoal | PredefinedQuantitativeGoal){
    this.nameInput = goal.name;
    this.descriptionInput = goal.description;
    this.goalInput = goal.description;
    this.daysInput = goal.days;
    this.stepInput = goal.step;
  }
  
  addTarget(){
    if(this.isQuantitative){
      this.targetService.addQuantitativeGoal({
        "id_user" : this.authService.currentUser.id,
        "is_shared": this.sharedInput,
        "name": this.nameInput,
        "description": this.descriptionInput,
        "goal": this.goalInput,
        "days": this.daysInput,
        "step": this.stepInput
      }).subscribe(
        response => {
          this.toastr.success("Udało się dodać cel zaliczeniowy!");
          this.router.navigate(['welcome']);
        },
        error => {
          this.toastr.error("Nie udało się dodać celu zaliczeniowego!");
        }
      );
    }
    else{ // ZALICZENIOWY
      this.targetService.addFinalGoal({
        "id_user" : this.authService.currentUser.id,
        "is_shared": this.sharedInput,
        "name": this.nameInput,
        "description": this.descriptionInput,
        "goal": this.goalInput,
        "days": this.daysInput
      }).subscribe(
        response => {
          this.toastr.success("Udało się dodać cel zaliczeniowy!");
          this.router.navigate(['welcome']);
        },
        error => {
          this.toastr.error("Nie udało się dodać celu zaliczeniowego!");
        }
      );
    }
  }
}
