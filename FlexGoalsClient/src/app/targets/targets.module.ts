import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowTargetsComponent } from './show-targets/show-targets.component';
import { PreviewTargetComponent } from './preview-target/preview-target.component';
import { AddTargetComponent } from './add-target/add-target.component';
import { FormsModule } from '@angular/forms';
import { EditTargetComponent } from './edit-target/edit-target.component';
import { ProgressbarModule } from "ngx-bootstrap/progressbar";
import { ShowPathsComponent } from './show-paths/show-paths.component';

@NgModule({
  declarations: [
    ShowTargetsComponent, 
    PreviewTargetComponent, 
    AddTargetComponent, 
    EditTargetComponent, 
    ShowPathsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ProgressbarModule.forRoot(),
  ],
  exports: [
    ShowTargetsComponent, 
    PreviewTargetComponent, 
    AddTargetComponent,
    EditTargetComponent,
    ShowPathsComponent
  ]
})
export class TargetsModule { }
