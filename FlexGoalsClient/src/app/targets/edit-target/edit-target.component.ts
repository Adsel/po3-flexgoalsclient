import {Component, OnInit, Input, Output, EventEmitter, OnDestroy} from '@angular/core';
import { TargetsService } from '../targets.service';
import { AuthService } from 'src/app/core/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { PredefinedFinalGoal, FinalGoal, PredefinedQuantitativeGoal, QuantitativeGoal } from 'src/app/model';

@Component({
  selector: 'tar-edit-target',
  templateUrl: './edit-target.component.html',
  styleUrls: ['./edit-target.component.scss']
})
export class EditTargetComponent implements OnInit,OnDestroy {
  isCollapsed = true;
  nameInput: string;
  descriptionInput: string;
  stepInput: number;
  goalInput: string;
  daysInput: number;
  sharedInput: boolean;
  isQuantitative: boolean;
  private id: number;

  @Input() editableFGoal: FinalGoal;
  @Input() editableQGoal: QuantitativeGoal;
  @Output() shift = new EventEmitter<string>();

  constructor(
    private route: ActivatedRoute,
    private targetService: TargetsService,
    private toastr: ToastrService
  ) {}


  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");

    this.route.params.subscribe(
      params => {
        console.log(params);
      }
    );
  }

  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  backToWelcome(){
    this.shift.emit('clr');
  }
  updateTarget(){
    if(this.editableQGoal){
      this.targetService.updateQuantitativeGoal({
        id: this.editableQGoal.id,
        name: this.editableQGoal.name,
        description: this.editableQGoal.description,
        goal: this.editableQGoal.goal,
        days: this.editableQGoal.days,
        date: null,
        progress: null,
        points: null,
        id_user: null,
        is_shared: this.editableQGoal.is_shared,
        step: this.editableQGoal.step,
        target: null
      }).subscribe(
        response => {
          console.log("Udało się zaktualizować cel ilościowy!");
        },
        error => {
          this.toastr.warning("Nie udało się aktualizować celu ilościowego!");
        }
      );
      this.editableQGoal = null;
      this.shift.emit('upd-clr');
    }
    else{ 
      this.targetService.updateFinalGoal({
        id: this.editableFGoal.id,
        name: this.editableFGoal.name,
        description: this.editableFGoal.description,
        goal: this.editableFGoal.goal,
        days: this.editableFGoal.days,
        date: null,
        progress: null,
        points: null,
        id_user: null,
        is_shared: this.editableFGoal.is_shared
      }).subscribe(
        response => {
          console.log("Udało się zaktualizować cel zaliczeniowy!");
        },
        error => {
          this.toastr.warning("Nie udało się zaktualizować celu zaliczeniowego!");
        }
      );
      this.editableFGoal = null;
      this.shift.emit('upd-clr');
    }
  }
}
