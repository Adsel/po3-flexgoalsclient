import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/auth.service';
import { TargetsService } from '../targets.service';
import { Path } from 'src/app/model';
import { Router } from '@angular/router';

@Component({
  selector: 'tar-show-paths',
  templateUrl: './show-paths.component.html',
  styleUrls: ['./show-paths.component.scss']
})
export class ShowPathsComponent implements OnInit {
  paths: Path[];
  nums: number[];

  constructor(
    private toastr: ToastrService,
    private authService: AuthService,
    private targetService: TargetsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadPaths();
  }

  private loadPaths() {
    this.targetService.getPaths().subscribe(
      response => {
        this.paths = response;
        this.countPaths();
        console.log("Udało się pobrać ścieżki!");
      },
      err => {
        this.toastr.warning("Nie udało się pobrać ścieżek!");
      }
    )
  }

  private countPaths(): void {
    this.nums = new Array();
    for(let i = 0; i < this.paths.length; i++){
      let temp = this.paths[i].path;
      let flag = false;
      for(let j = 0; j < this.nums.length; j++){
        if(this.nums[j] == temp){
          flag = true;
          break;
        }
      }
      if(!flag){
        this.nums.push(temp);
      }
    }
  }

  setPath(id: number ){
    this.targetService.setPath({
      "id_user": this.authService.currentUser.id,
      "id_path": id
    }).subscribe(
      response => {
        this.toastr.success("Udało się ustawić ścieżkę");
        this.emit();
      },
      err => {
        this.toastr.warning("Nie udało się ustawić ścieżki");
      }
    );
  }

  @Output() shift = new EventEmitter<string>();


  emit() {
    this.shift.emit('refresh');
  }

}
