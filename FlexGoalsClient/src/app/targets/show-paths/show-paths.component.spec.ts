import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPathsComponent } from './show-paths.component';

describe('ShowPathsComponent', () => {
  let component: ShowPathsComponent;
  let fixture: ComponentFixture<ShowPathsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPathsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPathsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
