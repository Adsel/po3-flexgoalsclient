import { Injectable, Inject } from '@angular/core';
import { CONFIG, Config, QuantitativeGoal, FinalGoal, PredefinedFinalGoal, FinalGoalData, PredefinedQuantitativeGoal, QuantitativeGoalData, ServerDate, FinalGoalFlag, QuantitativeGoalFlag, QuantitativeGoalProgress, Path, PathData } from '../model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TargetsService {
  constructor(
    @Inject(CONFIG) private config: Config,
    private httpClient: HttpClient
  ) { }

  getPredefinedFinalGoals(){
    return this.httpClient.get<PredefinedFinalGoal[]>(
      `${this.config.apiUrl}/goals/predefined-final`
    );
  }

  getPredefinedQuantitativeGoals(){
    return this.httpClient.get<PredefinedQuantitativeGoal[]>(
      `${this.config.apiUrl}/goals/predefined-quantitative`
    );
  }

  getFinalGoals(idUser: number){
    return this.httpClient.get<FinalGoalFlag[]>(
      `${this.config.apiUrl}/goals/final/${idUser}`
    );
  }

  getQuantitativeGoals(idUser: number){
    return this.httpClient.get<QuantitativeGoalFlag[]>(
      `${this.config.apiUrl}/goals/quantitative/${idUser}`
    );
  }  

  getSharedFinalGoals(idUser: number){
    return this.httpClient.get<FinalGoal[]>(
      `${this.config.apiUrl}/goals/final-shared/${idUser}`
    )
  }

  getSharedQuantitativeGoals(idUser: number){
    return this.httpClient.get<QuantitativeGoal[]>(
      `${this.config.apiUrl}/goals/quantitative-shared/${idUser}`
    )
  }

  addFinalGoal(finalGoal: FinalGoalData){
    return this.httpClient.post<FinalGoal>(
      `${this.config.apiUrl}/goals/final-add`, 
      finalGoal, 
      { headers: this.config.headerConfig }
    );
  }

  addQuantitativeGoal(finalGoal: QuantitativeGoalData){
    return this.httpClient.post<QuantitativeGoal>(
      `${this.config.apiUrl}/goals/quantitative-add`, 
      finalGoal, 
      { headers: this.config.headerConfig }
    );
  }

  deleteFinalGoal(id: number){
    return this.httpClient.delete(
      `${this.config.apiUrl}/goals/final-delete/${id}`,
      { headers: this.config.headerConfig }
    );
  }

  deleteQuantitativeGoal(id: number){
    return this.httpClient.delete(
      `${this.config.apiUrl}/goals/quantitative-delete/${id}`,
      { headers: this.config.headerConfig }
    );
  }

  getCurrentServerDate(){
    return this.httpClient.get<ServerDate>(
      `${this.config.apiUrl}/datetime/date`
    );
  }

  updateQuantitativeGoal(goal: QuantitativeGoal){
    return this.httpClient.put<QuantitativeGoal>(
      `${this.config.apiUrl}/goals/update-qgoaldata`,
      goal,
      { headers: this.config.headerConfig }
    );
  }

  updateFinalGoal(goal: FinalGoal){
    return this.httpClient.put<FinalGoal>(
      `${this.config.apiUrl}/goals/update-fgoal`,
      goal,
      { headers: this.config.headerConfig }
    );
  }

  doneFinalGoal(id: number){
    return this.httpClient.put<number>(
      `${this.config.apiUrl}/goals/update-prog-fgoal`,
      id,
      { headers: this.config.headerConfig }
    );
  }

  doneQuantitativeGoal(data: QuantitativeGoalProgress){
    return this.httpClient.put<number>(
      `${this.config.apiUrl}/goals/update-qgoal`,
      data,
      { headers: this.config.headerConfig }
    );
  }

  getFinalWithId(id: number){
    return this.httpClient.get<FinalGoal>(
      `${this.config.apiUrl}/goals/final-id/${id}`
    );
  }

  getQuantitativeWithId(id: number){
    return this.httpClient.get<QuantitativeGoal>(
      `${this.config.apiUrl}/goals/quantitative-id/${id}`
    );
  }

  getPaths(){
    return this.httpClient.get<Path[]>(
      `${this.config.apiUrl}/paths/predefined`
    );
  }  

  setPath(data: PathData){
    return this.httpClient.post<number>(
      `${this.config.apiUrl}/paths/set`, 
      data, 
      { headers: this.config.headerConfig }
    )
  }
}
