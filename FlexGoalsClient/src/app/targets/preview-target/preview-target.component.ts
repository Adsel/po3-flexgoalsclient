import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import Chart from "chart.js";
import { ActivatedRoute, Router } from '@angular/router';
import { TargetsService } from '../targets.service';
import { ToastrService } from 'ngx-toastr';
import { FinalGoal, QuantitativeGoal } from 'src/app/model';

@Component({
  selector: 'tar-preview-target',
  templateUrl: './preview-target.component.html',
  styleUrls: ['./preview-target.component.scss']
})
export class PreviewTargetComponent implements OnInit {
  isCollapsed = true;
  private progress: string;
  private sub: any;
  public type: string;
  private id: number;
  public goalQ: QuantitativeGoal;
  public goalF: FinalGoal;
  private nums: number[];
  private labels: string[];
  private suggestMax: number;
  value: number;

  constructor(
    private route: ActivatedRoute,
    private targetService: TargetsService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {

    this.loadData();
    //this.progress = 
  }

  private loadData(){
    this.nums = new Array();
    this.labels = new Array();
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.type = params['type'];
      console.log(this.id + " / " + this.type);
    });
    if(this.type === 'f'){
      this.targetService.getFinalWithId(this.id).subscribe(
        response => {
          this.goalF = response;
          this.estaminateF();
          console.log("Udało się pobrać pojedynczy cel!");
        },
        err => {
          this.toastr.warning("Nie udało się pobrać pojedynczego celu!");
        }
      );
    }
    else{
      this.targetService.getQuantitativeWithId(this.id).subscribe(
        response => {
          this.goalQ = response;
          this.estaminateQ();
          this.drawChart();
          console.log("Udało się pobrać pojedynczy cel!");
        },
        err => {
          this.toastr.warning("Nie udało się pobrać pojedynczego celu!");
        }
      );
    }
  }

  private estaminateQ(){
    let progressStr = this.goalQ.progress;
    let str = "";
    for(let i = 0; i < progressStr.length; i++){
      if(progressStr.charAt(i) == ','){
        this.nums[this.nums.length] = parseInt(str);
        str = "";
      }
      else{
        str += progressStr.charAt(i);
      }
    }
    this.nums[this.nums.length] = parseInt(str);

    for(let i = 0; i < this.nums.length; i++){
      this.labels[this.labels.length] = "Dzień " + (i + 1);
    }
  }

  private estaminateF(){
    let progressStr = this.goalF.progress;
    let str = "";
    for(let i = 0; i < progressStr.length; i++){
      if(progressStr.charAt(i) == ','){
        this.nums[this.nums.length] = parseInt(str);
        str = "";
      }
      else{
        str += progressStr.charAt(i);
      }
    }
    this.nums[this.nums.length] = parseInt(str);

    let nmb = 0;
    for(let i = 0; i < this.nums.length; i++){
      nmb += this.nums[i];
    }
    this.value = parseInt((nmb / this.nums.length * 100).toString())
    console.log(nmb + " / " + this.nums.length + "=" + this.value + " S");
  }

  private drawChart(){
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("landing-page");

    var canvas: any = document.getElementById("chartBig");
    var ctx = canvas.getContext("2d");
    var gradientFill = ctx.createLinearGradient(0, 350, 0, 50);
    gradientFill.addColorStop(0, "rgba(228, 76, 196, 0.0)");
    gradientFill.addColorStop(1, "rgba(228, 76, 196, 0.14)");
    var chartBig = new Chart(ctx, {
      type: "line",
      responsive: true,
      data: {
        labels: this.labels,
        datasets: [
          {
            label: "Data",
            fill: true,
            backgroundColor: gradientFill,
            borderColor: "#e44cc4",
            borderWidth: 2,
            borderDash: [],
            borderDashOffset: 0.0,
            pointBackgroundColor: "#e44cc4",
            pointBorderColor: "rgba(255,255,255,0)",
            pointHoverBackgroundColor: "#be55ed",
            //pointHoverBorderColor:'rgba(35,46,55,1)',
            pointBorderWidth: 20,
            pointHoverRadius: 4,
            pointHoverBorderWidth: 15,
            pointRadius: 4,
            data: this.nums
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: false
        },

        tooltips: {
          backgroundColor: "#fff",
          titleFontColor: "#ccc",
          bodyFontColor: "#666",
          bodySpacing: 4,
          xPadding: 12,
          mode: "nearest",
          intersect: 0,
          position: "nearest"
        },
        responsive: true,
        scales: {
          yAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: "rgba(0,0,0,0.0)",
                zeroLineColor: "transparent"
              },
              ticks: {
                display: false,
                suggestedMin: 0,
                suggestedMax: this.goalQ.step,
                padding: 20,
                fontColor: "#9a9a9a"
              }
            }
          ],

          xAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: "rgba(0,0,0,0)",
                zeroLineColor: "transparent"
              },
              ticks: {
                padding: 20,
                fontColor: "#9a9a9a"
              }
            }
          ]
        }
      }
    });
  }

  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("landing-page");
  }

  backToWelcome(){
    this.router.navigate(['welcome']);
  }
}
